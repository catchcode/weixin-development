# 微信公众号开发
## 配置
在更目录创建 `.env` 文件，并按照 `.env.example` 配置
> 直接将 `.env.example` 重命名成 `.env` 也可以

## 命令
### 运行
```cmd
npm run start
```
### 更新并运行
```cmd
npm pro
```
等同于：`npx forever stop wx & git pull && npm i && npm run start`   
翻译：停止`wx` & 拉取代码 && 安装依赖 && 启动`wx`
> &: 无论前面的命令是否执行成功都执行后面的代码
> &&: 前面的命令执行成功才执行后面的代码


### 停止
```cmd
npm run stop
```

### 拉取
```cmd
git pull
```

### 状态
你可通过直接访问主机监听的端口来确定是否启动成功
正常情况下，将显示欢迎语句
> 欢迎来到你的weixin-nodejs项目

### 日志
项目根目录的 `wx_out.log`
> 如果它占用空间很大，你应该手动清理它

