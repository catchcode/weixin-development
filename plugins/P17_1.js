const { Plugin } = require('weixin-nodejs')

class P17_1 extends Plugin {
    test() {
        return this.req.is('text') && this.params.Content == '17.1'
    }
    async process() {

        try {

            const create_tag = await Promise.all([
                this.app.tag.createTag('同学'),
                this.app.tag.createTag('室友'),
            ])

            await this.app.custom.typing(this.params.FromUserName);
            await this.app.custom.sendText({ touser: this.params.FromUserName, text: { content: '创建标签:' + JSON.stringify(create_tag) } })

            await this.app.custom.typing(this.params.FromUserName);
            await this.app.custom.sendText({ touser: this.params.FromUserName, text: { content: '获取标签:' + JSON.stringify(await this.app.tag.getTag()) } })

            await this.app.custom.typing(this.params.FromUserName);
            await this.app.custom.sendText({ touser: this.params.FromUserName, text: { content: '修改标签:' + JSON.stringify(await this.app.tag.updateTag({ tag: { id: create_tag[0].tag.id, name: '好友' } })) } })


            return '17.1正常结束结束';

        } catch (e) {
            return JSON.stringify(e);
        }

    }
}

module.exports = P17_1;