const { Plugin } = require('weixin-nodejs')

const axios = require('axios').default

const URL = 'https://v0.yiketianqi.com/api';

const INFO = {
    appid: "77699953",
    appsecret: "RyriO5y7",
    version: "v10",
}

/**
 * 发送内容“淮南天气”
 * 发送内容“蚌埠天气”
 */

module.exports = class P21 extends Plugin {
    test() {
        return this.req.is('text') && /.+?天气/.test(this.params.Content)
    }
    async process() {
        const { data } = await axios.get(URL, {
            params: {
                ...INFO,
                city: this.params.Content.substr(0, this.params.Content.length - 2),
            }
        })
        return `${data.city}:${data.air_tips}\npm2.5:${data.pm25}${data.pm25_desc}`
    }
}