const { Plugin } = require('weixin-nodejs')

const fs = require('fs')

const path = require('path')

class P12_4 extends Plugin {
    test() {
        return this.req.is('text') && this.params.Content == '客服图片'
    }
    async process() {
        const data1 = await this.media.upload('image', fs.createReadStream(path.resolve(__dirname, '../', 'file', '1.jpg')))

        const data2 = await this.custom.send({ image: { media_id: data1.media_id }, msgtype: 'image', touser: this.params.FromUserName })
        // const data2 = await this.custom.send({ image: 'text', text: 'hello' })

        const str = `${JSON.stringify(data1)}\n-------\n${JSON.stringify(data2)}`
        return this.reply.text(str)
    }
}

module.exports = P12_4