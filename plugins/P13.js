const { Plugin } = require('weixin-nodejs')

const _axios = require('axios')
const axios = _axios.default.create()

// 百度地图参数
const ak = process.env.baidu_ak


class P13 extends Plugin {
    test() {
        return this.req.isEvent('LOCATION');
    }

    async process() {
        const { data } = await axios.get(`http://api.map.baidu.com/reverse_geocoding/v3`, {
            params: {
                ak,
                output: 'json',
                coordtype: 'wgs84ll',
                location: this.params.Latitude + ',' + this.params.Longitude
            }
        })
        return this.reply.text(data.result.formatted_address)
    }
}

module.exports = P13