const { Plugin } = require('weixin-nodejs')

class P17_2 extends Plugin {
    test() {
        return this.req.is('text') && this.params.Content == '17.2'
    }
    async process() {

        try {
            const { tags } = await this.app.tag.getTag();
            const { data: { openid } } = await this.app.user.getBatchUser()

            openid.length = 2;// 只需要2个用户

            // // 为自己打上好友标签
            const res = await this.app.tag.batchTagGing({ openid_list: openid, tagid: tags.find(e => e.name == '好友').id })
            await this.app.custom.sendText({ touser: this.params.FromUserName, text: { content: `给${openid.join(',')}打上标签，\r\n${JSON.stringify(res)}` } })

            //查询某一关注用户身上的标签列表并输出
            const search = await this.app.tag.getUserTags(openid[0])
            await this.app.custom.sendText({ touser: this.params.FromUserName, text: { content: `${openid[0]} 身上的标签有：，\r\n${JSON.stringify(search)}` } })

            // // 获取“好友”标签下的用户列表并输出。

            const users = await this.app.tag.getTagUser({ tagid: tags.find(e => e.name == '好友').id })
            await this.app.custom.sendText({ touser: this.params.FromUserName, text: { content: `好友标签下有用户：${JSON.stringify(users)}` } })

        } catch (e) {
            console.log(e);
            return JSON.stringify(e);
        }

    }
}

module.exports = P17_2;