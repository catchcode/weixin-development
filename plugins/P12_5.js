/*
 * @Descripttion: 实训12-临时素材
 * @version: 1.0
 * @Author: 自由如风
 * @Date: 2021-05-07 08:10:07
 * @LastEditors: 自由如风
 * @LastEditTime: 2021-05-19 20:53:08
 */

const { Plugin } = require('weixin-nodejs')

const fs = require('fs')

const path = require('path')

class P12_5 extends Plugin {
    test() {
        return this.req.is('text') && this.params.Content == '下载素材图片'
    }
    async process() {
        const data1 = await this.media.upload('image', fs.createReadStream(path.resolve(__dirname, '../', 'file', '1.jpg')))
        console.log(`https://api.weixin.qq.com/cgi-bin/media/get?access_token=${this.access_token}&media_id=${data1.media_id}`);
        return this.reply.text('已从控制台打印')
    }
}


module.exports = P12_5
