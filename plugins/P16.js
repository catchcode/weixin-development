const { Plugin } = require('weixin-nodejs')

class P16 extends Plugin {
    test() {
        return this.req.is('text') && this.params.Content == '群发文本'
    }
    async process() {

        const user_list = await this.app.user.getBatchUser();

        try {

            const res = await this.app.mass.sendTextByOpenId({ touser: user_list.data.openid, text: { content: '这是群发文本' } });

            console.log(res);

            return '已向所有粉丝发送文本';
        } catch (e) {

            console.log(e);
            
            return JSON.stringify(e);
        }

    }
}

module.exports = P16;