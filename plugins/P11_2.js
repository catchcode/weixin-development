const { Plugin } = require('weixin-nodejs')

class P11_2 extends Plugin {
    test() {
        return this.req.is('text') && this.params.Content == '正在输入'
    }
    async process() {
        return JSON.stringify(await this.custom.typing(this.params.FromUserName))
    }
}

module.exports = P11_2