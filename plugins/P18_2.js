const { Plugin } = require('weixin-nodejs')

/**
 * 
 * 用户未关注时：
 * 事件为 subscribe 且附带 EventKey 字段（非扫码关注不带）
 * 用户关注时:
 * 事件为 SCAN 且附带 EventKey 字段
 * 
 * 总结，必须带有 EventKey 字段
 */

module.exports = class P18_2 extends Plugin {
    test() {
        return this.req.is('event') && !!this.params.EventKey
    }
    async process() {
        if (this.params.Event == 'subscribe') {
            return this.reply.text(`欢迎关注\n来自二维码场景\n${this.params.EventKey}`)
        } else if (this.params.Event == 'SCAN') {
            return this.reply.text(`扫描二维码场景\n${this.params.EventKey}`)
        }
        // 其他情况不处理
    }
}