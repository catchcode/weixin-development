const { Plugin } = require('weixin-nodejs')

const SEX = ['未知', '男', '女']

class P14 extends Plugin {
    // 禁用
    static disable = true;
    test() {
        return this.req.is('event') && this.req.isEvent('subscribe')
    }
    async process() {
        const info = await this.app.user.getInfo(this.params.FromUserName);
        let text = '';
        text += `你好:${info.nickname}\n`
        text += `性别:${SEX[info.sex]}\n`
        text += `地区:${info.city}\n`
        text += `语言:${info.language}\n`
        text += `关注:${info.subscribe_time}`
        return text;
    }
}

module.exports = P14;