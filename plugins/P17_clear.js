const { Plugin } = require('weixin-nodejs')

module.exports = class extends Plugin {
    test() {
        return this.req.is('text') && this.params.Content == '清除标签'
    }
    async process() {
        try {
            await this.app.tag.getTag().then(async ({ tags }) => {
                tags = tags.filter(e => e.id >= 100);
                for (const tag of tags) {
                    await this.app.tag.delTag(tag.id)
                }
            })
            return '已删除所有标签';
        } catch (e) {
            console.log(e);
            return JSON.stringify(e);
        }

    }
}