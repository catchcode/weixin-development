const { Plugin } = require('weixin-nodejs')

class P17_3 extends Plugin {
    test() {
        return this.req.is('text') && this.params.Content == '17.3'
    }
    async process() {
        try {
            await this.app.mass.sendTextByTag({ filter: { is_to_all: true }, text: { content: "hello[群发消息]" } })
        } catch (e) {
            console.log(e);
            return JSON.stringify(e);
        }

    }
}

module.exports = P17_3;