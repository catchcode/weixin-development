const { Plugin } = require('weixin-nodejs')

module.exports = class P18_1_1 extends Plugin {
    test() {
        return this.req.is('text') && this.params.Content == '18.1.1'
    }
    async process() {
        // 创建二维码
        const qrcode = await this.app.qrcode.create({ action_name: "QR_STR_SCENE", action_info: { scene: "二维码" } })
        // 获取二维码地址
        const url = await this.app.qrcode.getUrl(qrcode.ticket)
        // 返回二维码
        return this.reply.news({ PicUrl: url, Description: "二维码", Title: "二维码", Url: url })
    }
}