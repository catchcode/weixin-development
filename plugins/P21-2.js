const { Plugin } = require('weixin-nodejs')

const axios = require('axios').default

const URL = 'http://api.qingyunke.com/api.php';

const PARAMS = {
    key: "free",
    appid: "0"
}

/**
 * 发送内容“翻译I love you”
 * 发送内容“翻译module”
 */

module.exports = class P21_2 extends Plugin {
    test() {
        return this.req.is('text') && /翻译.+/.test(this.params.Content)
    }
    async process() {
        const { data } = await axios.get(URL, {
            params: {
                ...PARAMS,
                msg: this.params.Content,
            }
        })
        return JSON.stringify(data)
    }
}