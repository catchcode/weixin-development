const { Plugin } = require('weixin-nodejs')

/**
 * 1.在测试号管理页面添加模板：
 * 模板标题：艺术培训
 * 模板内容：
 *              {{first.DATA}}
                课单单号: {{keyword1.DATA}} 
                课单金额: {{keyword2.DATA}} 
                选购课程: {{keyword3.DATA}} 
                学习时间: {{keyword4.DATA}} 
                老师联系方式: {{keyword5.DATA}} 
                时间: {{keyword6.DATA}} 
                {{remark.DATA}}
 */


/** 模板创建后，将模板ID复制到这里 */
const TemplateID = '9Ly1vpr4-OwgMX3AjgXOPBMXMu65NYO6MqSxb5YXs-4'

module.exports = class P20 extends Plugin {
    test() {
        return this.req.is('text') && this.params.Content == '模板消息';
    }
    async process() {
        const template = await this.app.template.send({
            data: {
                first: { value: `恭喜${this.params.FromUserName}选课成功` },
                keyword1: { value: 123456789 },
                keyword2: { value: `${parseInt(Math.random() * 1600)}元` },
                keyword3: { value: "钢琴" },
                keyword4: { value: (new Date().getMonth() + 1) + "月" },
                keyword5: { value: 110120119 },
                keyword6: { value: new Date().toLocaleString() },
                remark: { value: '欢迎下次购买！' },
            },
            template_id: TemplateID,
            touser: this.params.FromUserName,
        })
        return JSON.stringify(template);
    }


}