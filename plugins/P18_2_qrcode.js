const { Plugin } = require('weixin-nodejs')

const fs = require('fs')
const path = require('path')

/**
 * 创建 18.2任务中所需要的二维码。
 */

module.exports = class P18_2_qrcode extends Plugin {
    test() {
        return this.req.is('text') && this.params.Content == '18.2.qrcode'
    }
    async process() {

        const { qrcode: QR } = this.app;

        // 获取二维码
        const qrcode = await QR.create({ action_info: { scene: { scene_str: "1000" } }, action_name: 'QR_STR_SCENE' })
        // 获取二维码下载器
        const downloader = await QR.getDownloader(qrcode.ticket)
        // 拼接路径
        const _path = path.join(process.cwd(), 'file', 'qrcode_1000.jpg')
        // 使用下载器将文件保存至硬盘
        await downloader.save(_path)
        // 延迟1秒（太快容易报错）
        await async function () {
            return new Promise(resolve => {
                setTimeout(resolve, 1000)
            })
        }()
        // 上传素材
        const media = await this.app.media.uploadImage(fs.createReadStream(_path))
        // 返回二维码图片
        return this.reply.image(media.media_id);
    }
}