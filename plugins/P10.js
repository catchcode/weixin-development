const { Plugin } = require('weixin-nodejs')

class P10 extends Plugin {
    test() {
        return this.req.is('voice');
    }
    process() {
        if (/文本/.test(this.params.Recognition)) {
            return '文本消息';
        } else if (/图文/.test(this.params.Recognition)) {
            return this.reply.news({ Title: 'Title', Description: 'Description', PicUrl: 'PicUrl', Url: 'http://baidu.com' })
        } else if (/音乐/.test(this.params.Recognition)) {
            return this.reply.music({ HQMusicUrl: 'http://music.codeon.cn/1.mp3', MusicUrl: 'http://music.codeon.cn/1.mp3', Title: '音乐', Description: 'Description' })
        } else {
            return `文本/语音消息不是预期内容:${this.params.Recognition}`
        }
    }
}
module.exports = P10