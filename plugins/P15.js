const { Plugin } = require('weixin-nodejs')

const axios = require('axios').default

class P15 extends Plugin {
    test() {
        return this.req.is('text') && this.params.Content == '用户列表'
    }
    async process() {
        const user_list = await this.app.user.getBatchUser();

        return user_list.data.openid.join('\n')
    }
}

module.exports = P15;