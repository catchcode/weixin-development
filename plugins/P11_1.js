const { Plugin } = require('weixin-nodejs')

class P11_1 extends Plugin {
    test() {
        return this.req.is('text') && this.params.Content == '客服消息'
    }
    async process() {
        return JSON.stringify(await this.app.custom.send({ touser: this.params.FromUserName, msgtype: 'text', text: { content: '客服消息' } }));
    }
}
module.exports = P11_1