const { Plugin } = require('weixin-nodejs')

const fs = require('fs');

const path = require('path')

module.exports = class P18_1_2 extends Plugin {
    test() {
        return this.req.is('text') && this.params.Content == '18.1.2'
    }
    async process() {
        // 创建二维码
        const qrcode = await this.app.qrcode.create({ action_name: "QR_STR_SCENE", action_info: { scene: "保存到本地" } })
        // 获取二维码数据
        const data = await this.app.qrcode.get(qrcode.ticket)
        // 存储路径
        const _path = '../file/qrcode.jpg';
        // 写入文件
        fs.writeFileSync(path.join(__dirname, _path), data, { flag: 'w+' })
        return this.reply.text(`操作成功，文件存路径${_path}`)
    }
}