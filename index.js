﻿const { WX, Plugin } = require('weixin-nodejs')

const dotenv = require('dotenv')
const path = require('path')

dotenv.config()

const wx = new WX({
    token: process.env.token,
    port: process.env.port,
    appid: process.env.appid,
    secret: process.env.secret,
    defaultContent: '',
    plugin_path: path.join(__dirname, 'plugins')
})